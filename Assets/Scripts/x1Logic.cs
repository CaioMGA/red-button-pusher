﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class x1Logic : MonoBehaviour {

	/* Game
	 * 1 Start Round
	 *     Instantiate player objects and init variables
	 * 2 Wait for score
	 *     Register Score
	 * 3 Someone scored
	 *     Register points
	 *     Check victory conditions
	 *     If no one won, continue game
	 *     Kill players objects
	 * 1 Start Round
	 *   ...
	 */
	public GameObject [] playersPrefabs;
	public GameObject [] playersOnScreen;
	public GameObject [] dancingAvatars;
	public GameObject CollisionParticles;
	public int winCondition = 5;
	public int p1Points = 0;
	public int p2Points = 0;
	public Text p1UIScore;
	public Text p2UIScore;
	public GameObject countdownPanel;
	public Transform cameraTiraTeima;
	public GameObject [] holdLabel;
	public GameObject [] holdIndicator;

	x1Camera x1camera;
	WinPanelLogic winPanel;
	HistoryLog log;
	AudioControl audioControl;

	Animator [] holdLabelAnim;

	float scoreDelay = 1;
	float countdownDelay = 4.2f;
	float deltatime = 0;
	float holdingButtonTime = 3f;
	float P1HoldingButtonDeltatime = 0;
	float P2HoldingButtonDeltatime = 0;
	float postRoundDelay = 1;

	PlayerMovement p1Mov;
	PlayerMovement p2Mov;

	bool p1Wins = false;
	bool p2Wins = false;

	bool p1Ready = false;
	bool p2Ready = false;

	string gamestate = "preround";
	string [] holdLabelText = {"Hold any key to continue", "Ready"};


	void Start(){
		playersOnScreen = new GameObject[2];
		x1camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<x1Camera>();
		winPanel = GetComponent<WinPanelLogic>();
		log = GetComponent<HistoryLog>();
		audioControl = GetComponent<AudioControl>();
		holdLabelAnim = new Animator[2];
		holdLabelAnim[0] = holdLabel[0].GetComponent<Animator>();
		holdLabelAnim[1] = holdLabel[1].GetComponent<Animator>();
	}

	void initPlayers(){
		playersOnScreen[0] = Instantiate(playersPrefabs[0]);
		p1Mov = playersOnScreen[0].transform.GetChild(0).GetComponent<PlayerMovement>();
		p1Ready = false;

		playersOnScreen[1] = Instantiate(playersPrefabs[1]);
		p2Mov = playersOnScreen[1].transform.GetChild(0).GetComponent<PlayerMovement>();
		p2Ready = false;

		x1camera.players[0] = playersOnScreen[0].transform.GetChild(0).transform;
		x1camera.players[1] = playersOnScreen[1].transform.GetChild(0).transform;

		disableMovement();
	}

	void clearStage(){
		foreach(GameObject go in playersOnScreen){
			Destroy(go);
		}
		showHoldButtonLabels(false);
		showHoldButtonIndicators(false, 0);
		showHoldButtonIndicators(false, 1);
	}

	void Update(){
		if(gamestate.Equals("preround")){
			testWinCondition();
			clearStage();
			if(!gamestate.Equals("winmessage")){
				updateHUD();
				initPlayers();
				//resetPlayersDirectionals();
				audioControl.playFight();
				x1camera.resetTransform();
				countdownPanel.SetActive(false);
				countdownPanel.SetActive(true);
				CollisionParticles.SetActive(false);
				deltatime = 0;
				gamestate = "round";
				x1camera.fight = true;
			}
		}
		if(gamestate.Equals("round")){
			if(deltatime > countdownDelay){
				//activate players' movement
				enableMovement();
				gamestate = "fight";
			} else {
				deltatime += Time.deltaTime;
			}
		}

		if(gamestate.Equals("postround")){
			updateHUD();
			//3 seconds delay
			if(deltatime > scoreDelay){
				gamestate = "waitingplayers";
				showHoldButtonLabels(true);
				holdLabel[0].GetComponent<Text>().text = holdLabelText[0];
				holdLabel[1].GetComponent<Text>().text = holdLabelText[0];
				Debug.Log("Waiting players");
			} else {
				deltatime += Time.deltaTime;
			}
		}

		if(gamestate.Equals("waitingplayers")){
			checkPlayersAreReady();
		}

		if(gamestate.Equals("winmessage")){
			if(Input.anyKeyDown){
				winPanel.showGameOverMenu(true);
			}
		}
	}

	public void score(int player, Vector3 colPosition){
		if(gamestate =="fight"){
			log.registerVictory(player);
			cameraTiraTeima.position = new Vector3(colPosition.x, colPosition.y, colPosition.z);
			x1camera.lookAt(cameraTiraTeima);
			CollisionParticles.transform.position = cameraTiraTeima.position;
			CollisionParticles.SetActive(true);
			freeze();
			gamestate = "processingresults";
			if(player==1){
				p1Points += 1;
			} else if(player ==2){
				p2Points += 1;
			} else {
				Debug.Log("Can't score to player " + player);
			}
			//updateHUD();
			//testWinCondition();
			gamestate = "postround";
			audioControl.playRoundEnd();
			deltatime = 0;
		}
	}

	void updateHUD(){
		p1UIScore.text = p1Points.ToString();
		p2UIScore.text = p2Points.ToString();
	}

	void testWinCondition(){

		if(p1Points == winCondition){
			p1Wins = true;
		}
		if(p2Points == winCondition){
			p2Wins = true;
		}

		if(p1Wins && p2Wins){
			// Draw
			// Show draw screen
			disableMovement();
			showVictoryDance();
		} else if(p1Wins){
			// P1 wins
			//Show p1 Victory Screen
			disableMovement();
			showVictoryDance();
		} else if(p2Wins){
			// P2 wins
			//Show p2 Victory Screen
			disableMovement();
			showVictoryDance();
		}

	}

	void disableMovement(){
		p1Mov.enabled = false;
		p2Mov.enabled = false;
	}

	void enableMovement(){
		p1Mov.enabled = true;
		p2Mov.enabled = true;
	}

	void showVictoryDance(){
		audioControl.playGameover();
		int winner = 0;
		if(p1Wins && p2Wins){
			winner = 0;
		} else if(p1Wins){
			winner = 1;
		} else if(p2Wins){
			winner = 2;
		}

		playersOnScreen[0].SetActive(false);
		playersOnScreen[1].SetActive(false);
		CollisionParticles.SetActive(false);

		if(p1Wins){
			dancingAvatars[0].SetActive(true);
		}
		if(p2Wins){
			dancingAvatars[1].SetActive(true);
		}
		cameraTiraTeima.position = Vector3.zero;
		x1camera.lookAt(cameraTiraTeima);
		gamestate = "winmessage";
		winPanel.showMsg(winner);
	}

	void freeze(){
		foreach(GameObject p in playersOnScreen){
			Rigidbody [] rbs = p.GetComponentsInChildren<Rigidbody>();
			foreach(Rigidbody rb in rbs){
				rb.isKinematic = true;
			}
		}
	}

	void checkPlayersAreReady(){
		if(!p1Ready){
			if(Input.GetButton("P1 AnyKey")){
				P1HoldingButtonDeltatime += Time.deltaTime;
				showHoldButtonIndicators(true, 0);
			} else {
				P1HoldingButtonDeltatime = 0;
				showHoldButtonIndicators(false, 0);
			}
		}

		if(!p2Ready){
			if(Input.GetButton("P2 AnyKey")){
				P2HoldingButtonDeltatime += Time.deltaTime;
				showHoldButtonIndicators(true, 1);
			} else {
				P2HoldingButtonDeltatime = 0;
				showHoldButtonIndicators(false, 1);
			}
		}

		if(P1HoldingButtonDeltatime >= holdingButtonTime){
			p1Ready = true;
			holdLabel[0].GetComponent<Text>().text = holdLabelText[1];
		}

		if(P2HoldingButtonDeltatime >= holdingButtonTime){
			p2Ready = true;
			holdLabel[1].GetComponent<Text>().text = holdLabelText[1];
		}

		if(p1Ready && p2Ready){
			if(deltatime > postRoundDelay){
				gamestate = "preround";
			} else {
				deltatime += Time.deltaTime;
			}

		} else {
			deltatime = 0;
		}
	}

	void showHoldButtonLabels(bool value){
		holdLabel[0].SetActive(value);
		holdLabel[1].SetActive(value);
	}

	void showHoldButtonIndicators(bool value, int index){
		if(holdIndicator[index].activeInHierarchy != value){
			holdIndicator[index].SetActive(value);
			if(value == true){
				holdLabelAnim[index].SetTrigger("HOLD");
			} else {
				holdLabelAnim[index].SetTrigger("WAIT");
			}
		}
	}
}
