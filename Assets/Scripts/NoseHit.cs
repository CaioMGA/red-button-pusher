﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoseHit : MonoBehaviour {

	public bool player1;
	//public Material noseHitMaterial;
	x1Logic logic;

	void Start(){
		logic = GameObject.FindGameObjectWithTag("GameLogic").GetComponent<x1Logic>();
	}

	void OnCollisionEnter(Collision other){
		Debug.Log(other.transform.tag);
		if(other.transform.CompareTag("Nose")){
			//other.transform.GetComponent<Renderer>().material = noseHitMaterial;
			scorePoint(other.contacts[0].point);
		}
	}

	void scorePoint(Vector3 colPosition){
		if(player1){
			logic.score(1, colPosition);

		} else {
			logic.score(2, colPosition);
		}
	}
}
