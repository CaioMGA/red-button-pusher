﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControl : MonoBehaviour {

	public AudioClip fight;
	public AudioClip roundEnd;
	public AudioClip gameover;

	AudioSource audioSource;

	void Start(){
		audioSource = GetComponent<AudioSource>();
	}

	public void playFight(){
		audioSource.Stop();
		audioSource.clip = fight;
		audioSource.Play();
	}

	public void playRoundEnd(){
		audioSource.Stop();
		audioSource.clip = roundEnd;
		audioSource.Play();
	}

	public void playGameover(){
		audioSource.Stop();
		audioSource.clip = gameover;
		audioSource.Play();
	}


}
