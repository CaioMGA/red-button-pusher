﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class x1Camera : MonoBehaviour {
	public Transform cameraCloseUp;
	public Transform cameraMaxDistance;
	public float cameraProgress = 0;
	public Transform cameraRotationPivot;
	public Transform cameraAim;
	public Transform [] players;
	public float orbitingSpeed = 5;
	public float maxPlayersDistance = 75;
	public float maxCameraZDepth = -43;

	Transform cameraTarget;
	Vector3 startPosition;
	Quaternion startRotation;
	float offset = 3;
	public float playersDistance = 0;

	public bool orbiting = false;
	public bool fight = false;


	void Start(){
		startPosition = transform.position;
		startRotation = transform.rotation;
		players = new Transform[2];
	}

	void FixedUpdate () {
		if(orbiting){
			transform.LookAt(cameraTarget);
			cameraRotationPivot.Rotate(0, orbitingSpeed * Time.deltaTime, 0);
		} else if(fight){
			if(transform.parent == null){
				maxCameraZDepth = Vector3.Distance(cameraAim.position, transform.position);
				transform.parent = cameraAim;
			}
			solveFraming();
			cameraAim.position = (players[0].transform.position + players[1].transform.position) * 0.5f;
			playersDistance = Vector3.Distance(players[0].position, players[1].position);
			cameraProgress = (playersDistance - 2.7f) / 72.3f;
		}


	}

	public void lookAt(Transform target){
		transform.position = new Vector3(target.position.x, target.position.y + offset, target.position.z + offset);
		cameraRotationPivot.position = new Vector3(target.position.x, 0, 0);
		transform.parent = cameraRotationPivot;
		orbiting = true;
		cameraTarget = target;
	}

	public void resetTransform(){
		transform.position = startPosition;
		transform.rotation = startRotation;
		transform.parent = null;
		orbiting = false;
		cameraAim.position = (players[0].transform.position + players[1].transform.position) * 0.5f;
	}

	void solveFraming(){
		transform.position = Vector3.Lerp(cameraCloseUp.position, cameraMaxDistance.position, cameraProgress);
		transform.rotation = Quaternion.Lerp(cameraCloseUp.rotation, cameraMaxDistance.rotation, cameraProgress);
	}
}
