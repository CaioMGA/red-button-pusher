﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HistoryLog : MonoBehaviour {

	public Image [] roundBadges;
	public Sprite [] winBadges;

	int round = 0;

	public void registerVictory(int winner){
		roundBadges[round].sprite = winBadges[winner];
		round ++;
	}
}
