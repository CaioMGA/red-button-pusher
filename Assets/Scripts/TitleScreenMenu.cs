﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScreenMenu : MonoBehaviour {

	public GameObject aboutPanel;
	public GameObject promptQuitPanel;

	public void newGame(){
		//SceneManager.LoadScene(1);
		SceneManager.LoadSceneAsync(1);
	}

	public void about(bool value){
		aboutPanel.SetActive(value);
	}

	public void promptQuit(){
		promptQuitPanel.SetActive(true);
	}

	public void quitYes(){
		Application.Quit();
	}

	public void quitNo(){
		promptQuitPanel.SetActive(false);
	}

	public void openURL(string url){
		Application.OpenURL(url);
	}
}
