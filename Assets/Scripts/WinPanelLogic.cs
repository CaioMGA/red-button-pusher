﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WinPanelLogic : MonoBehaviour {
	public GameObject winMessage;
	public GameObject gameOverMenu;
	public Text msg;
	public Text titleMenu;

	public void showMsg(int winner){
		if(winner == 0){
			msg.text = "Draw!";
		}
		if(winner == 1){
			msg.text = "Player 1 wins!";
		}
		if(winner == 2){
			msg.text = "Player 2 wins!";
		}
		if(winner > 2){
			msg.text = "ERROR";
		}
		titleMenu.text = msg.text;
		winMessage.SetActive(true);
	}

	public void showGameOverMenu(bool value){
		gameOverMenu.SetActive(value);
		winMessage.SetActive(!value);
	}

	public void rematch(){
		SceneManager.LoadScene(1);
	}

	public void mainMenu(){
		SceneManager.LoadScene(0);
	}

}
