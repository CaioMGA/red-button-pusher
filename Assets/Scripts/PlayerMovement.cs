﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
	public float speed = 1800;
	public bool player1;
	float xAxis;
	Rigidbody rb;

	void Start(){
		rb = GetComponent<Rigidbody>();
	}

	void getInputs(){
		xAxis = 0;
		if(player1){
			if(Input.GetKey(KeyCode.A)){
				xAxis -= 1;
			}
			if(Input.GetKey(KeyCode.D)){
				xAxis += 1;
			}
		}

		else {
			if(Input.GetKey(KeyCode.LeftArrow)){
				xAxis -= 1;
			}
			if(Input.GetKey(KeyCode.RightArrow)){
				xAxis += 1;
			}
		}
	}

	void move(){
		rb.AddForce(new Vector3(speed * xAxis, 0, 0));
	}

	void FixedUpdate(){
		getInputs();
		move();
	}
}
